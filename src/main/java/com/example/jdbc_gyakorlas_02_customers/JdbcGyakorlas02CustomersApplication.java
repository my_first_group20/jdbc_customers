package com.example.jdbc_gyakorlas_02_customers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JdbcGyakorlas02CustomersApplication {

    public static void main(String[] args) {
        SpringApplication.run(JdbcGyakorlas02CustomersApplication.class, args);
    }

}
