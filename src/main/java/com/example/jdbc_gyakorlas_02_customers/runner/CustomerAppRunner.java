package com.example.jdbc_gyakorlas_02_customers.runner;

import com.example.jdbc_gyakorlas_02_customers.model.Customer;
import com.example.jdbc_gyakorlas_02_customers.repository.customer.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class CustomerAppRunner implements ApplicationRunner {

    private final CustomerRepository customerRepository;

    @Autowired
    public CustomerAppRunner(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
//        Customer customer = new Customer(0, "John", "Doe", "Hungary", "1234", "0630123456", "johndoe@emai.com");
//        Customer customerUpdate = new Customer(60, "Jane", "Doe", "Hungary", "1234", "0630123456", "johndoe@emai.com");
//        customerRepository.insert(customer);

//        System.out.println(customerRepository.getPageOfCustomers(5, 2));

//        customerRepository.update(customerUpdate);

//        System.out.println(customerRepository.findCountryWithMostCustomers());
//        System.out.println(customerRepository.findHighestSpenderCustomer());
//        System.out.println(customerRepository.findMostPopularGenre(6));
//        System.out.println(customerRepository.findByName("Jan", "Doe"));
        customerRepository.deleteById(60);
    }
}
