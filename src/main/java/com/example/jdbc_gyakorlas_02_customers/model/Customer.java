package com.example.jdbc_gyakorlas_02_customers.model;

public record Customer(int id,
                       String firstName,
                       String lastName,
                       String country,
                       String postalCode,
                       String phoneNumber,
                       String email) {
}
