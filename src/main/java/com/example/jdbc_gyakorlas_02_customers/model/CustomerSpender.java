package com.example.jdbc_gyakorlas_02_customers.model;

public record CustomerSpender(int customerId,
                              String firstName,
                              String lastName,
                              double totalSpending) {
}
