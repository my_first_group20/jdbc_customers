package com.example.jdbc_gyakorlas_02_customers.model;

public record CustomerCountry(String countryName) {
}
