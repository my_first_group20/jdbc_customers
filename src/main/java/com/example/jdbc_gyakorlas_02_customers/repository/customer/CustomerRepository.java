package com.example.jdbc_gyakorlas_02_customers.repository.customer;

import com.example.jdbc_gyakorlas_02_customers.model.Customer;
import com.example.jdbc_gyakorlas_02_customers.model.CustomerCountry;
import com.example.jdbc_gyakorlas_02_customers.model.CustomerGenre;
import com.example.jdbc_gyakorlas_02_customers.model.CustomerSpender;
import com.example.jdbc_gyakorlas_02_customers.repository.CRUDRepository;

import java.util.List;

public interface CustomerRepository extends CRUDRepository<Customer, Integer> {

    Customer findByName(String firstName, String lastName);

    List<Customer> getPageOfCustomers(int limit, int offset);

    CustomerCountry findCountryWithMostCustomers();

    CustomerSpender findHighestSpenderCustomer();

    List<CustomerGenre> findMostPopularGenre(int customerId);
}
