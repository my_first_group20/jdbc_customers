package com.example.jdbc_gyakorlas_02_customers.repository.customer;

import com.example.jdbc_gyakorlas_02_customers.model.Customer;
import com.example.jdbc_gyakorlas_02_customers.model.CustomerCountry;
import com.example.jdbc_gyakorlas_02_customers.model.CustomerGenre;
import com.example.jdbc_gyakorlas_02_customers.model.CustomerSpender;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository {

    private final String url;
    private final String username;
    private final String password;

    public CustomerRepositoryImpl(@Value("${spring.datasource.url}") String url,
                                 @Value("${spring.datasource.username}") String username,
                                 @Value("${spring.datasource.password}")String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }
    
    @Override
    public List<Customer> findAll() {
        String sql = "SELECT * FROM customer";
        List<Customer> customers = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(url, username, password)){

            //write prepared statement
            PreparedStatement statement = conn.prepareStatement(sql);

            //execute prepared statement
            ResultSet result = statement.executeQuery();

            //handle result
            while (result.next()) {                 //result.next() loops through all the rows in the ResultSet
                Customer customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
                customers.add(customer);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return customers;
    }

    @Override
    public Customer findById(Integer id) {
        String sql  = "SELECT * FROM customer WHERE customer_id = ?";
        Customer customer = null;
        
        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            //write prepared statement
            PreparedStatement statement = conn.prepareStatement(sql);

            //set parameter, index starts with 1, not 0!!!
            statement.setInt(1, id);

            //execute prepared statement
            ResultSet result = statement.executeQuery();
  
            if (result.next()) {
                customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
            } 

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    @Override
    public int insert(Customer customer) {
        String sql = "INSERT INTO customer (first_name, last_name, country, postal_code, phone, email) VALUES (?,?,?,?,?, ?)";
        int result = 0;
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);

            statement.setString(1, customer.firstName());
            statement.setString(2, customer.lastName());
            statement.setString(3, customer.country());
            statement.setString(4, customer.postalCode());
            statement.setString(5, customer.phoneNumber());
            statement.setString(6, customer.email());
            // Execute statement
            result = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public int update(Customer customer) {

        String sql = "UPDATE customer SET first_name = ?, last_name = ?, country = ?, postal_code = ?, phone = ?, email = ? WHERE  customer_id = ?";
        int result = 0;
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);

            statement.setString(1, customer.firstName());
            statement.setString(2, customer.lastName());
            statement.setString(3, customer.country());
            statement.setString(4, customer.postalCode());
            statement.setString(5, customer.phoneNumber());
            statement.setString(6, customer.email());
            statement.setInt(7, customer.id());
            // Execute statement
            result = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public int delete(Customer customer) {
        String sql  = "DELETE FROM customer WHERE customer_id = ?";
        int result = 0;

        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            //write prepared statement
            PreparedStatement statement = conn.prepareStatement(sql);

            //set parameter, index starts with 1, not 0!!!
            statement.setInt(1, customer.id());

            //execute prepared statement
            result = statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public int deleteById(Integer id) {

        String sql  = "DELETE FROM customer WHERE customer_id = ?";
        int result = 0;

        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            //write prepared statement
            PreparedStatement statement = conn.prepareStatement(sql);

            //set parameter, index starts with 1, not 0!!!
            statement.setInt(1, id);

            //execute prepared statement
            result = statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public Customer findByName(String firstName, String lastName) {
        String sql  = "SELECT * FROM customer WHERE first_name LIKE ? AND last_name LIKE ?";
        Customer customer = null;

        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            //write prepared statement
            PreparedStatement statement = conn.prepareStatement(sql);

            //set parameter, index starts with 1, not 0!!!
            statement.setString(1, "%" + firstName + "%");
            statement.setString(2, "%" + lastName + "%");

            //execute prepared statement
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    @Override
    public List<Customer> getPageOfCustomers(int limit, int offset) {
        String sql = "SELECT * FROM customer LIMIT ? OFFSET ?";
        List<Customer> pageOfCustomers = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(url, username, password)){

            //write prepared statement
            PreparedStatement statement = conn.prepareStatement(sql);

            statement.setInt(1, limit);
            statement.setInt(2, offset);

            //execute prepared statement
            ResultSet result = statement.executeQuery();

            //handle result
            while (result.next()) {                 //result.next() loops through all the rows in the ResultSet
                Customer customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
                pageOfCustomers.add(customer);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return pageOfCustomers;
    }

    @Override
    public CustomerCountry findCountryWithMostCustomers() {
        String sql  = "SELECT count(customer_id), Country FROM customer GROUP BY Country ORDER BY count DESC LIMIT 1;";
        CustomerCountry mostCustomers = null;

        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            //write prepared statement
            PreparedStatement statement = conn.prepareStatement(sql);

            //execute prepared statement
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                mostCustomers = new CustomerCountry(result.getString("country"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return mostCustomers;
    }

    @Override
    public CustomerSpender findHighestSpenderCustomer() {
        String sql  = "SELECT invoice.total, invoice.customer_id, customer.first_name, customer.last_name "
                + "FROM invoice INNER JOIN customer ON customer.customer_id = invoice.customer_id" +
                " ORDER BY invoice.total DESC LIMIT 1";
        CustomerSpender highestSpender = null;

        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            //write prepared statement
            PreparedStatement statement = conn.prepareStatement(sql);

            //execute prepared statement
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                highestSpender = new CustomerSpender(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getDouble("total")
                );
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return highestSpender;
    }

    //ToDo: can't handle tie:
    @Override
    public List<CustomerGenre> findMostPopularGenre(int customerId) {
        String sql  = "SELECT count(Customer_id), genre_count.genre_name " +
                "FROM (SELECT invoice.invoice_id, invoice.customer_id, invoice_line.invoice_line_id, track.track_id, genre.name as genre_name " +
                "FROM invoice " +
                "INNER join customer " +
                "ON customer.customer_id = invoice.customer_id " +
                "Inner join invoice_line " +
                "ON invoice_line.invoice_id = invoice.invoice_id " +
                "Inner join track " +
                "ON invoice_line.track_id = track.track_id " +
                "Inner join genre " +
                "ON genre.genre_id = track.genre_id " +
                "Where customer.customer_id = ?) as genre_count " +
                "GROUP BY genre_count.genre_name " +
                "Order by count desc " +
                "Limit 1";

        List<CustomerGenre> popularGenres = new ArrayList<>();

        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            //write prepared statement
            PreparedStatement statement = conn.prepareStatement(sql);

            statement.setInt(1, customerId);

            //execute prepared statement
            ResultSet result = statement.executeQuery();

            while (result.next()) {                 //result.next() loops through all the rows in the ResultSet
                CustomerGenre popularGenre = new CustomerGenre(
                        result.getString("genre_name")
                );
                popularGenres.add(popularGenre);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return popularGenres;
    }
}
